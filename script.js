window.addEventListener("load", (event) => {
    let animatedBox = document.getElementsByClassName('animated-box');
    let links = document.querySelectorAll('.link');

    for (let i = 0; i < links.length; i++) {
        links[i].addEventListener("mouseenter", (e) => videoFunction(e));
    }
    if (animatedBox) {
        animatedBox[0].className += " animate";
    }
    let toggle = document.getElementById('toggle');
    toggle.addEventListener("click", (e) => menuFunction(e));
});


function videoFunction(e) {
    let id = e.toElement.id;
    let current = document.getElementById('video'+id);
    let videos = document.getElementsByClassName('video active');

    if (!current.classList.contains("active")) {
        videos[0].classList.remove("active");
        current.classList.add("active");
    }
}

function menuFunction(e) {
    e.preventDefault();
    let toggle = document.getElementById('toggle');
    let menu = document.getElementById('menu');
    if (toggle.classList.contains("opened")) {
        toggle.classList.remove("opened");
        menu.classList.remove("open");
    }
    else {
        toggle.classList.add("opened")
        menu.classList.add("open");
    }
}
